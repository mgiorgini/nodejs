const express = require('express');
const router = express.Router();

const proveedores = require('../proveedoresBd');

const getProveedores = () => {
    return proveedores;
}

const insertProveedor = (proveedor) => {
    return proveedores.push(proveedor);
}

const updateProveedorById = (proveedorId, proveedor) => {
    const upProve = proveedores.filter( (proveed) => proveed.id != proveedorId);
    upProve.push(proveedor);
    return upProve;
}

const deleteProveedorById = (proveedorId) => {
    return proveedores.filter( (proveedor) => proveedor.id != proveedorId);
}


//Rutas
//1) get que obtenga todos los proveedores
router.get('/', function (req, res) {
    res.json(getProveedores());
})

//2) post que agregue un proveedor
router.post('/', function (req, res) {
    let proveedor = insertProveedor(req.body.proveedor);
    res.json(proveedor);
})

//3) put que actualice un proveedor
router.put('/:id', function(req, res) {
    let proveedor = updateProveedorById(req.params.id, req.body.proveedor); 
    res.json(proveedor);
})

//4) delete que borre un proveedor
router.delete('/:id', function(req, res) {
    let proveedor = deleteProveedorById(req.params.id);
    res.json(proveedor);   
})

module.exports = router;