const express = require('express');
const router = express.Router();

const usuarios = require('../usersBd');


const getUsuarioById = (userId) => {
    return usuarios.find( (user) => user.id == userId );
}

const getUsuarios = () => {
     return usuarios;
}

function findByIdAndPosition(id, position) {
    return usuarios.find( (user ) => user.id == id && user.job.position == position );
}

// Routes
router.get('/', function (req, res) {
    res.json(getUsuarios());
})

router.get('/:id', function (req, res) {
    console.log(req.params.name);
    let user = getUsuarioById(req.params.id);
     res.json(user);
})

router.post('/', function (req, res) {
    let user = req.body.user;
    console.log(user);
    usuarios.push(user);
    res.json(user);
})

router.get('/:id/job/:position', function(req,res) {
    let user = findByIdAndPosition(req.params.id, req.params.position);
    if(user) {
        res.json(user)
    } else {
        res.send("no existe tal descripcion")
    }
})

module.exports = router