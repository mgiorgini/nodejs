const express = require('express');
const router = express.Router();
const productos = require('../productsBd');

const getProductos = () => {
    return productos;
}

const getProductoById = (productId) => {
    return productos.find( (product) => product.id == productId );
}

function findProdByIdAndProveed(id, name) {
    return productos.find( (product ) => product.id == id && product.proveedor.name == name );
}

const getProductosByProveed = (name) => {
    return productos.filter( (product) => product.proveedor.name == name );
}

const getProductosPrecioMayorA = (precio) => {
    return productos.filter( (product) => product.proveedor.precio > precio );
}

const deleteProductoById = (productId) => {
    return productos.filter( (product) => product.id != productId);
}

const updateProductoById = (productId, product) => {
    const upProducts = productos.filter( (products) => products.id != productId);
    upProducts.push(product);
    console.log(upProducts); //borrar luego y hacer return en el push
    return upProducts;
}

//1) crear la ruta para obtener todos los productos
router.get('/', function (req, res) {
    res.json(getProductos());
})

//2) crear la ruta para obtener un producto por Id
router.get('/:id', function (req, res) {
    let product = getProductoById(req.params.id);
    res.json(product);
})

//3) crear la ruta para obtener un producto por Id y nombre de provedor
router.get('/:id/proveedor/:name', function(req,res) {
    let product = findProdByIdAndProveed(req.params.id, req.params.name);
    if(product) {
        res.json(product)
    } else {
        res.send("No existe elemento buscado")
    }
})

//4) crear la ruta para Agregar este  Producto
router.post('/', function (req, res) {
    let product = req.body.product;
    console.log(product);
    productos.push(product);
    res.json(product);
})

//5) crear la ruta para obtener todos los productos que pertenezcan a dulcenter
router.get('/proveedor/:name', function (req, res) {
    let product = getProductosByProveed(req.params.name);
    res.json(product);
})

//6) crear la ruta para obtener todos los productos que valen mas de 100 pesos
router.get('/proveedor/precio/precio', function (req, res) {
    let product = getProductosPrecioMayorA(req.query.gt);
    res.json(product);
})

//7) crear la ruta para modificar un producto
router.put('/:id', function(req, res) {
    let product = updateProductoById(req.params.id, req.body.product); 
    res.json(product);
})

//8) crear la ruta para borrar un producto 
router.delete('/:id', function(req, res) {
    let product = deleteProductoById(req.params.id);
    console.log(product);
    res.json(product);   
})

module.exports = router;