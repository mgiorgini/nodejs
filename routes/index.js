const router = require('express').Router()

const rutasUsers = require('./users');
const rutasProducts = require('./products');
const rutasProveedores = require('./proveedores');

router.use('/users', rutasUsers);
router.use('/products', rutasProducts);
router.use('/proveedores', rutasProveedores);

router.get('/', function (req, res) {
    res.status(200).json({message: 'Estás conectado a nuestra API'})
})

module.exports = router