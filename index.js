const express = require('express')
const app = express() 

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:true})); // Ya podemos recibir datos post
app.use(bodyParser.json());
app.use(bodyParser.raw());

const routes = require('./routes');
app.use('/', routes);

//Middlewares


app.listen(3000, () => {
    console.log('Servidor escuchando por el puerto 3000');
});