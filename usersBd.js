module.exports = [
    {
        id:1,
        name:"Bruno",
        surname:"Mendez",
        job: {
            id: 1,
            position: "fullstack"
        }
    },
    {
        id:2,
        name:"Leonardo",
        surname:"Mendez",
        job: {
            id: 2,
            position: "Developer"
        }
    }
]